# == Schema Information
#
# Table name: items
#
#  created_at   :datetime         not null
#  cupboard_id  :integer
#  description  :string
#  id           :integer          not null, primary key
#  item_type    :integer
#  name         :string
#  product_code :string
#  quantity     :integer
#  updated_at   :datetime         not null
#

module ItemsHelper
end
