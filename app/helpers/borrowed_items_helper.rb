# == Schema Information
#
# Table name: borrowed_items
#
#  created_at  :datetime         not null
#  fine        :decimal(10, 2)
#  id          :integer          not null, primary key
#  item_id     :integer
#  returned_at :datetime
#  updated_at  :datetime         not null
#  user_id     :integer
#

module BorrowedItemsHelper
  def dynamic_borrowed_items_label
    current_user.officer? ? 'Borrowed Items' : 'Borrow Items'
  end

  def dynamic_returned_at_value borrowed_item
    if borrowed_item.returned?
      "#{borrowed_item.returned_at.strftime("%b %e, %Y")}"
    else
      "N/A"
    end
  end

  def dynamic_borrow_link item
    path = item.out_of_stock? ? '#' : borrow_path(item)
    method = item.out_of_stock? ? :get : :post
    link_to 'Borrow', path, method: method, class: 'label'
  end

  def dynamic_return_link borrowed_item
    path = borrowed_item.returned? ? '#' : return_path(borrowed_item)
    method = borrowed_item.returned? ? :get : :post
    link_to 'Returned', path, method: method, class: 'label'
  end
end
