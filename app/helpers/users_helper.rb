# == Schema Information
#
# Table name: users
#
#  active                 :boolean
#  created_at             :datetime         not null
#  current_sign_in_at     :datetime
#  current_sign_in_ip     :inet
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  first_name             :string
#  id                     :integer          not null, primary key
#  last_name              :string
#  last_sign_in_at        :datetime
#  last_sign_in_ip        :inet
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  role                   :string
#  sign_in_count          :integer          default(0), not null
#  uid                    :string
#  updated_at             :datetime         not null
#

module UsersHelper
  def dynamic_edit_user_link
    if params[:id].present?
      link_to 'Edit', edit_user_path(@user), class: 'button'
    else
      link_to 'Edit', edit_profile_path, class: 'button'
    end
  end

  def account_status user
    user.active? ? 'Active' : 'Deactivated'
  end
end
