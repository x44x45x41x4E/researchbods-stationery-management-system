# == Schema Information
#
# Table name: borrowed_items
#
#  created_at  :datetime         not null
#  fine        :decimal(10, 2)
#  id          :integer          not null, primary key
#  item_id     :integer
#  returned_at :datetime
#  updated_at  :datetime         not null
#  user_id     :integer
#

class BorrowedItemsController < ApplicationController
  before_action :set_item, only: [:borrow]
  before_action :set_borrowed_item, only: [:return]

  # GET /borrowed_items
  def index
    @items = Item.by_cupboard.by_name
    @borrowed_items = if current_user.officer?
                        BorrowedItem.recently_updated
                      else
                         current_user.borrowed_fixed_assets
                      end

     @fixed_assets = BorrowedItem.latest_fixed_assets
     @consumables = BorrowedItem.latest_consumables
  end

  def borrow
    unless current_user.fined?
      if current_user.borrow @item
        redirect_to borrowed_items_url, notice: 'Item successfully borrowed'
      else
        redirect_to borrowed_items_url, notice: 'Item failed to be borrowed'
      end
    else
      redirect_to borrowed_items_url, notice: 'Settle your fines first'
    end
  end

  def return
    if current_user.return @borrowed_item
      redirect_to borrowed_items_url, notice: 'Item successfully returned'
    else
      redirect_to borrowed_items_url, notice: 'Item failed to be returned'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    def set_borrowed_item
      @borrowed_item = BorrowedItem.find(params[:id])
    end

end
