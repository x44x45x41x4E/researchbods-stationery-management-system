# == Schema Information
#
# Table name: items
#
#  created_at   :datetime         not null
#  cupboard_id  :integer
#  description  :string
#  id           :integer          not null, primary key
#  item_type    :integer
#  name         :string
#  product_code :string
#  quantity     :integer
#  updated_at   :datetime         not null
#

class ItemsController < ApplicationController
  before_action :ensure_officer
  before_action :set_item, only: [:show, :edit, :update, :destroy]

  # GET /items
  def index
    @items = Item.by_name
  end

  # GET /items/1
  def show
  end

  # GET /items/new
  def new
    @item = Item.new
  end

  # GET /items/1/edit
  def edit
  end

  # POST /items
  def create
    @item = Item.new(item_params)

    if @item.save
      redirect_to items_url, notice: 'Item was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /items/1
  def update
    if @item.update(item_params)
      redirect_to items_url, notice: 'Item was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /items/1
  def destroy
    @item.destroy
    redirect_to items_url, notice: 'Item was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def item_params
      params.require(:item).permit(:name, :description, :cupboard_id, :quantity, :item_type, :product_code)
    end
end
