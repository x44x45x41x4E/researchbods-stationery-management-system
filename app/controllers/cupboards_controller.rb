# == Schema Information
#
# Table name: cupboards
#
#  created_at  :datetime         not null
#  description :text
#  id          :integer          not null, primary key
#  name        :string
#  updated_at  :datetime         not null
#

class CupboardsController < ApplicationController
  before_action :ensure_officer
  before_action :set_cupboard, only: [:edit, :update, :destroy]

  # GET /cupboards
  def index
    @cupboards = Cupboard.all
  end

  # GET /cupboards/new
  def new
    @cupboard = Cupboard.new
  end

  # GET /cupboards/1/edit
  def edit
  end

  # POST /cupboards
  def create
    @cupboard = Cupboard.new(cupboard_params)

    if @cupboard.save
      redirect_to cupboards_url, notice: 'Cupboard was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cupboards/1
  def update
    if @cupboard.update(cupboard_params)
      redirect_to cupboards_url, notice: 'Cupboard was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cupboards/1
  def destroy
    @cupboard.destroy
    redirect_to cupboards_url, notice: 'Cupboard was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cupboard
      @cupboard = Cupboard.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cupboard_params
      params.require(:cupboard).permit(:name, :description)
    end
end
