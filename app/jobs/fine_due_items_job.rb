class FineDueItemsJob < ActiveJob::Base
  queue_as :default

  def perform(*args)
      BorrowedItem.latest_fixed_assets.each do |bi|
        bi.fine!
      end
  end
end
