# == Schema Information
#
# Table name: users
#
#  active                 :boolean
#  created_at             :datetime         not null
#  current_sign_in_at     :datetime
#  current_sign_in_ip     :inet
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  first_name             :string
#  id                     :integer          not null, primary key
#  last_name              :string
#  last_sign_in_at        :datetime
#  last_sign_in_ip        :inet
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  role                   :string
#  sign_in_count          :integer          default(0), not null
#  uid                    :string
#  updated_at             :datetime         not null
#

class User < ActiveRecord::Base
  before_create :generate_uid
  before_validation :set_default_role, on: :create
  before_validation :set_default_account_status, on: :create

  ROLES = %w{
    Borrower
    Officer
  }

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :borrowed_items
  has_many :items, through: :borrowed_items

  validates :role, presence: true, inclusion: { in: ROLES }
  validates_uniqueness_of :uid

  scope :recent, -> { order(updated_at: :desc) }
  scope :borrowers, -> { where(role: "Borrower") }

  ROLES.each do |role|
    define_method "#{role.downcase}?".to_sym do
      self.role == role
    end
  end

  def name
    "#{first_name.strip} #{last_name.strip}"
  end

  def borrowed_fixed_assets
    borrowed_items.map { |bi| bi if bi.fixed_asset? && !bi.returned? }.flatten.compact
  end

  def total_fines
    fines = 0
    borrowed_fixed_assets.map { |bi| fines += bi.fine if bi.due? && bi.fined? }.flatten.compact
    fines
  end

  def fined?
    self.total_fines > 0
  end

  def borrow item
    borrowed_item = BorrowedItem.new(item_id: item.id, user_id: self.id)
    borrowed_item.save
    item.decrement!(:quantity, 1)
  end

  def return borrowed_item
    return false unless borrowed_item
    if borrowed_item.fined?
      borrowed_item.remove_fine_and_return!
    else
      borrowed_item.return!
    end
    borrowed_item.item.increment!(:quantity, 1)
  end

  private
  def set_default_account_status
    self.active = true
  end

  def set_default_role
    self.role = "Borrower"
  end

  def generate_uid
    self.uid = loop do
      random_token = SecureRandom.hex(16)
      break random_token unless self.class.exists?(uid: random_token)
    end
  end
end
