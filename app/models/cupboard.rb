# == Schema Information
#
# Table name: cupboards
#
#  created_at  :datetime         not null
#  description :text
#  id          :integer          not null, primary key
#  name        :string
#  updated_at  :datetime         not null
#

class Cupboard < ActiveRecord::Base
  has_many :items
end
