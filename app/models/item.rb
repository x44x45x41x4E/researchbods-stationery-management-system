# == Schema Information
#
# Table name: items
#
#  created_at   :datetime         not null
#  cupboard_id  :integer
#  description  :string
#  id           :integer          not null, primary key
#  item_type    :integer
#  name         :string
#  product_code :string
#  quantity     :integer
#  updated_at   :datetime         not null
#

class Item < ActiveRecord::Base
  belongs_to :cupboard
  has_many :borrowed_items
  has_many :borrowers, through: :borrowed_items, source: :user

  enum item_type: [:fixed_asset, :consumable]

  validates :item_type, inclusion: { in: Item.item_types.keys }

  scope :recent, -> { order(created_at: :desc) }
  scope :by_name, -> { order(name: :asc) }
  scope :fixed_assets, -> { where(item_type: "fixed_asset") }
  scope :consumables, -> { where(item_type: "consumable") }
  scope :by_cupboard, -> { order(cupboard_id: :asc) }

  def out_of_stock?
    self.quantity == 0 ? true : false
  end
end
