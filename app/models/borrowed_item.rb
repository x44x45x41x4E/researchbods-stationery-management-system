# == Schema Information
#
# Table name: borrowed_items
#
#  created_at  :datetime         not null
#  fine        :decimal(10, 2)
#  id          :integer          not null, primary key
#  item_id     :integer
#  returned_at :datetime
#  updated_at  :datetime         not null
#  user_id     :integer
#

class BorrowedItem < ActiveRecord::Base
  before_create :set_borrowed_at_value, on: :save

  belongs_to :item
  belongs_to :user

  scope :recently_updated, -> { order(updated_at: :desc) }

  def self.latest_fixed_assets
    recently_updated.map { |bi| bi if bi.fixed_asset? }.flatten.compact
  end

  def self.latest_consumables
    recently_updated.map { |bi| bi if bi.consumable? }.flatten.compact
  end

  def borrowed_at
    self.created_at
  end

  def fixed_asset?
    item.fixed_asset?
  end

  def consumable?
    item.consumable?
  end

  def returned?
    self.returned_at.present?
  end

  def return!
    self.update(returned_at: DateTime.now)
  end

  def due_on
    self.borrowed_at + 2.days
  end

  def due?
    Date.today >= self.due_on.to_date
  end

  def exceeded?
    self.returned_at.to_date > self.due_on.to_date ? true : false
  end

  def fined?
    self.fine > 0
  end

  def fine!
    if self.due? && self.fixed_asset? && !self.returned?
      self.update(fine: 10.00)
    end
  end

  def remove_fine_and_return!
     if self.fixed_asset?
       self.update(fine: 0, returned_at: DateTime.now)
     end
  end

  private
  def set_borrowed_at_value
    self.created_at = DateTime.now
  end
end
