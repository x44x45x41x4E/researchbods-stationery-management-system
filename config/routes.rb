Rails.application.routes.draw do
  get 'borrowed_items/index'

  unauthenticated :user do
    devise_scope :user do
      root to: 'devise/sessions#new', as: 'unauthenticated_root'
    end
  end

  authenticated :user do
    root to: 'borrowed_items#index'
  end

  get 'profile', to: 'users#show', as: 'profile'
  get 'profile/edit', to: 'users#edit', as: 'edit_profile'
  put 'profile/update', to: 'users#update', as: 'update_profile'

  get 'borrowed_items', to: 'borrowed_items#index', as: 'borrowed_items'
  post 'borrow/:id', to: 'borrowed_items#borrow', as: 'borrow'
  post 'return/:id', to: 'borrowed_items#return', as: 'return'

  resources :users, except: [:destroy, :create]

  resources :items
  resources :cupboards, except: :show

  devise_for :users, path: 'auth', path_names: { sign_in: 'login', sign_out: 'logout' }
end
