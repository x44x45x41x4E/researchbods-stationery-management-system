# Add seed file for all envs here
models = %w{users cupboards items}

models.each do |model|
  puts '#' * 80
  puts "Table: #{model.upcase}"
  require_relative "models/#{model}.rb"
end
