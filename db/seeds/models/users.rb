users = [
  {
    :first_name => 'Neil',
    :last_name => 'Perry',
    :role => 'Borrower'
  },
  {
    :first_name => 'Todd',
    :last_name => 'Anderson',
    :role => 'Borrower'
  },
  {
    :first_name => 'John',
    :last_name => 'Keating',
    :role => 'Officer'
  }
]

ActiveRecord::Base.transaction do
  if User.all.empty?
    users.each do |user|
      new_user = User.new
      new_user.first_name = user[:first_name]
      new_user.last_name = user[:last_name]
      new_user.email = "#{user[:first_name].downcase}@researchbods.com"
      new_user.role = user[:role]
      new_user.password = 'ResearchBods2016'

      if new_user.save
        print '✓'
      else
        puts new_user.errors.inspect
        break
      end
    end
    print "\nTotal : #{User.all.count}\n"
  else
    print "Skipped seeding users.\n"
  end
end
