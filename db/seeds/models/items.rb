items = [
  {
    name: 'Post-it Notes',
    description: 'A small piece of paper with a re-adherable strip of glue on its back, made for temporarily attaching notes to documents and other surfaces',
    quantity: 100,
    cupboard_id: 1,
    item_type: 1
  },
  {
    name: 'Stapler',
    description: 'A mechanical device that joins pages of paper or similar material by driving a thin metal staple through the sheets and folding the ends',
    quantity: 2,
    cupboard_id: 1,
    item_type: 0
  },
  {
    name: 'Post-it Notes',
    description: 'A small piece of paper with a re-adherable strip of glue on its back, made for temporarily attaching notes to documents and other surfaces',
    quantity: 100,
    cupboard_id: 2,
    item_type: 1
  },
  {
    name: 'Stapler',
    description: 'A mechanical device that joins pages of paper or similar material by driving a thin metal staple through the sheets and folding the ends',
    quantity: 4,
    cupboard_id: 2,
    item_type: 0
  }
]

ActiveRecord::Base.transaction do
  if Item.all.empty?
    items.each do |item|
      new_item = Item.new
      new_item.product_code = SecureRandom.hex(3).capitalize
      new_item.name = item[:name]
      new_item.description = item[:description]
      new_item.quantity = item[:quantity]
      new_item.cupboard_id = item[:cupboard_id]
      new_item.item_type = item[:item_type]

      if new_item.save
        print '✓'
      else
        puts new_item.errors.inspect
        break
      end
    end
    print "\nTotal : #{Item.all.count}\n"
  else
    print "Skipped seeding items.\n"
  end
end
