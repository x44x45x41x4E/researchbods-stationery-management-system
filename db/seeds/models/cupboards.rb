cupboards = [
  {
    name: 'Cupboard #1'
  },
  {
    name: 'Cupboard #2'
  }
]

ActiveRecord::Base.transaction do
  if Cupboard.all.empty?
    cupboards.each do |cupboard|
      new_cupboard = Cupboard.new
      new_cupboard.name = cupboard[:name]

      if new_cupboard.save
        print '✓'
      else
        puts new_cupboard.errors.inspect
        break
      end
    end
    print "\nTotal : #{Cupboard.all.count}\n"
  else
    print "Skipped seeding cupboards.\n"
  end
end
