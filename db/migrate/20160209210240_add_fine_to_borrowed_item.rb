class AddFineToBorrowedItem < ActiveRecord::Migration
  def change
    add_column :borrowed_items, :fine, :decimal, precision: 10, scale: 2
  end
end
