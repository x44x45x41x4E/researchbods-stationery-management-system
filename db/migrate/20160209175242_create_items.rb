class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.string :description
      t.string :product_code
      t.integer :quantity
      t.references :cupboard, index: true, foreign_key: true
      t.integer :item_type

      t.timestamps null: false
    end
  end
end
