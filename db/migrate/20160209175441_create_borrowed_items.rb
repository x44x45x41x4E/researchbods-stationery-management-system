class CreateBorrowedItems < ActiveRecord::Migration
  def change
    create_table :borrowed_items do |t|
      t.references :item, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.datetime :returned_at

      t.timestamps null: false
    end
  end
end
