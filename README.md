ResearchBods Stationery Management System
=============================

### Specifications

- Built in RoR 4, Ruby 2
- No gem restrictions
- Use git for version control
- No fancy UI graphics design but something usable would, of course, be desirable

### Project Notes

A stationery management system that has cupboards wherein items are stored. Cupboards are currently two. An item could be a fixed asset or a consumable, latter is replenished from the suppliers. Maximum days that an item could be borrowed couldn’t be greater than 2. A registered user can use/borrow any item.

#### Requirements
1. Stationery products could be saved, amended and removed.
2. Users’ and their details could be saved, amended and deactivated.
3. Users are allowed to borrow and use an item.

##### Nice to have
1. Borrowing History of a user and an item can be saved and viewed. Much like a reports dashboard
2. Fine or Penalty Feature
    - Users are fined if they weren’t able to return an item on time
    - They won’t be able to borrow an item util they pay their fines
    - Fines could be n days times the fixed amount for penalty. Fixed amount could be changed depending on the stationary officer

### Features
1. User Registration, Authentication
    - Login
    - Signup
2. Stationary Management
  - Item CRUD
  - Cupboard CRUD
  - Borrowed Items Management

### Database Relationship

_User_
- Roles
    - Borrower
        - can borrow items
        - can edit own details
        - can check borrowing history
        - can return items
    - Stationary Officer
        - can add new items
        - can update existing item details and quantity
        - can delete an item
        - can deactivate a borrower’s account
- Attributes & Associations
    - uid:string
    - first_name:string
    - last_name:string
    - password:string
    - role:string
    - has_many :borrowed_items
    - has_many :items, through: :borrowed_items
    - active:boolean

_Cupboard_
- Attributes & Associations
    - name:string
    - description:text
    - has_many :items

_Item_
- Attributes & Associations
    - enum item_type: [:fixed_asset, :consumable]
    - name:string
    - description:text
    - product_code:string
    - quantity:integer
    - belongs_to :cupboard
    - has_many :borrowed_items
    - has_many :users, through: :borrowed_items

_Borrowed Items_
- Attributes & Associations
    - item_id:integer
    - user_id:integer
    - belongs_to :user
    - belongs_to :item
    - borrowed_at:datetime
    - returned_at:datetime
- Note
    - This could be used as well as the history for an item and a borrower’s borrowing history

### Others

#### Stationery Item Examples
- Pens
- Files
- Papers
- Erasers
- Markers
- Folders
- Envelopes
- Post-it Notepads
- Stapler
- Punchers

#### Recommended features
- Quotation feature for making purchase orders
- Suppliers module

### Setting up the Project

Assuming that you have the proper Ruby version installed, please run the following commands in order

1. `bundle install`
2. `bundle exec rake db:create db:migrate db:seed`

You can now proceed your testing by logging in using the credentials below or by registering a new user then logging in using your account.

#### Credentials
##### Officer Account
```
  email: john@researchbods.com
  password: ResearchBods2016
```

##### Borrower Account
```
email: todd@researchbods.com
password: ResearchBods2016
```
